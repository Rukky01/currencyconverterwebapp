let currencies = {
    "AUD" : "Australian Dollar",
    "BGN" : "Bulgarian Lev",
    "BRL" : "Brazilian Real",
    "CAD" : "Canadian Dollar",
    "CHF" : "Swiss Franc",
    "CNY" : "Chinese Yuan",
    "CZK" : "Czech Koruna",
    "DKK" : "Danish Krone",
    "EUR" : "Euro",
    "GBP" : "British Pound",
    "HKD" : "Hong Kong Dollar",
    "HRK" : "Croatian Kuna",
    "HUF" : "Hungarian Forint",
    "IDR" : "Indonesian Rupiah",
    "ILS" : "Israel Shekel",
    "INR" : "Indian Rupee",
    "ISK" : "Icelandic Krona",
    "JPY" : "Japanese Yen",
    "KRW" : "South Korean Won",
    "MXN" : "Mexican Peso",
    "MYR" : "Malaysian Ringgit",
    "NOK" : "Norwegian Krone",
    "NZD" : "New Zeland Dollar",
    "PHP" : "Philippene Peso",
    "PLN" : "Polish Zloty",
    "RON" : "Romanian Leu",
    "RUB" : "Russian Ruble",
    "SEK" : "Swedish Krona",
    "SGD" : "Singapore Dollar",
    "THB" : "Thai Baht",
    "TRY" : "Turkish Lira",
    "USD" : "US Dollar",
    "ZAR" : "South Africa Rand"
}

/* Input fields */
const inputAmount = document.getElementById("iAmount");
const fromCurrency = document.getElementById('select_from');
const toCurrency = document.getElementById('select_to');
/* Output fields */
const enteredAmount = document.getElementById('entered');
const calculatedAmount = document.getElementById('calculated');
const rates = document.getElementById('rates');
/* Button for combinations */
const combination_btn = document.getElementById('combination');
/* Modal */
const modal = document.getElementById('myModal');
const closeBtn = document.getElementsByClassName('close')[0];
const modalTitle = document.getElementById('modalTitle');
const modalTable = document.getElementById('modalTable');
const modalTableHead = document.getElementById('modalTableHead');

// Build dropdown menu with given data
function createDropDown()  {
    let html = ""
    Object.keys(currencies).forEach(element => {
        html += "<option value='" + element + "'>" + element + " - " + currencies[element] + "</option>\n"
    });

    const elements = document.getElementsByClassName("ddMenu");
    Array.prototype.slice.call( elements ).forEach(element => {
        element.innerHTML = html;
    });
}

// Called on calculate button
function calculate() {
    if (inputAmount.value.length < 1) {
        inputAmount.focus();
        enteredAmount.innerHTML = 'Please enter the amount!';
        calculatedAmount.innerHTML = '';
        rates.innerHTML = '';
        combination_btn.style.display = 'none';
        return;
    }
    getData(inputAmount.value, fromCurrency, toCurrency);
}

// Declared HTTP client
let HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        let anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
            aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open( "GET", aUrl, true ); 
        anHttpRequest.send(); 
    }
}

// Get data from API
function getData(amount, fromC, toC){
    let from =  fromC.options[fromC.selectedIndex].value;    
    let to =  toC.options[toC.selectedIndex].value;      
    let theurl='https://api.exchangeratesapi.io/latest?base=' + from;
    let client = new HttpClient();

    client.get(theurl, function(response) { 
        let response1 = JSON.parse(response);
        setResults(Math.round(amount * 100)/100, from, 
            Math.round(response1["rates"][to] * amount * 100)/100 , 
            to,Math.round(response1["rates"][to] * 100)/100 );

        let rates = Object.assign({}, response1["rates"]); 
        for (var key in rates) {
            if (rates.hasOwnProperty(key)) {
                rates[key] = Math.round(rates[key] * 100)/100;                
            }
        }
        buildModalData(from, rates);
    });    
}

// Insert rates in HTML
function setResults(fromAmount, fromCurrency, toAmount, toCurrency, rate) {
    enteredAmount.innerHTML = fromAmount + " " + fromCurrency + ' ='; 
    calculatedAmount.innerHTML = toAmount + ` <small>${toCurrency}</small>`
    const fromToRate = Math.round(1/rate * 100)/100;
    rates.innerHTML = `1 ${fromCurrency} = ${rate} ${toCurrency}<br>1 ${toCurrency} = ${fromToRate} ${fromCurrency}`;

    combination_btn.style.display = 'block';
}
// Switch currency in dropdowns
function switchCurrency() {
    const temp = fromCurrency.value;
    fromCurrency.value = toCurrency.value;
    toCurrency.value = temp;
}

// Call calculate function on key enter event
inputAmount.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    calculate();
  }
});

/* Display modal on click */
function displayModal() {
    modal.style.display = "block";
}
/* Close modal on click */
closeBtn.onclick = function() {
    modal.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
}
/* Build modal data */
function buildModalData(fromCurrency, rates) {
    modalTitle.innerHTML = `All combination with selected currency`
    modalTableHead.innerHTML = `<tr>
                                    <th colspan="2">
                                        1 ${fromCurrency} correspond
                                    </th>
                                </tr>`
    modalTable.innerHTML = '';
    let tempCell = [];

    for (var key in rates) {
        if (rates.hasOwnProperty(key)) {
            tempCell.push(`<td>${rates[key]} ${key}</td>`);            
        }
    }
    let j = 0;
    for (let i = 0; i < tempCell.length; i+=2) {

        if (tempCell[i+1]) {
            if (j%2 === 0) {
                modalTable.innerHTML += `<tr>
                                            ${tempCell[i]}
                                            ${tempCell[i+1]}
                                        </tr>`
            } else {
                modalTable.innerHTML += `<tr class="row-dark">
                                            ${tempCell[i]}
                                            ${tempCell[i+1]}
                                        </tr>`
            }
            
        } else {
            modalTable.innerHTML += `<tr>
                                        ${tempCell[i]}
                                    </tr>`
        }
        j++;
    }    
}

// Call function to build dropdown menu
createDropDown();