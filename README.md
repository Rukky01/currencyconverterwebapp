# Currency Converter Web App
This web app is developed as a faculty project. Developed purely with HTML, CSS, JS, and has fully responsive design.
It can be seen here: [Currency Converter](https://converter.lukaruskan.com/)

# Features!

  - Latest selected currency rates
  - Quick switch currency button
  - List of all rates of all combination of the currency


It uses free service for current and historical foreign exchange rates published by the European Central Bank:
`https://api.exchangeratesapi.io`

### Developed
This project is developed by [Luka Ruskan.](https://lukaruskan.com/)

### Preview
![Preview](preview.png)